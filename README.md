# mobility MD

Hello everyone this is the repository the Master of Science degree in Mobility Engineering. (Polimi) reserved by PoliNetwork.

You will be able to find and upload materials for your courses (Notes, Exercises, ...).

To upload files use the Telegram Bot [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

- First year https://gitlab.com/polinetwork/mobilityMDy1
 
- Second year https://gitlab.com/polinetwork/MobilityMDy2


`DISCLAIMER`
**You mustn't upload any copyright protected or in any other author protection form cover file on this repository or any of the ones associated to it.**

Polinetwork takes no responsibility for and we do not expressly or implicitly endorse, support, or guarantee the completeness, truthfulness, accuracy, or reliability of any of the content on this repository or any of the ones associated to it.

